from django.shortcuts import render
from django.core import serializers

# Create your views here.
from django.http import HttpResponse, JsonResponse
from basicauth.decorators import basic_auth_required
from django.contrib import admin
from django.shortcuts import redirect
from .models import Comments, ClientDetails
from datetime import datetime
import urllib2
import json
from django.views.decorators.csrf import csrf_protect
import timeago

def reformat_str(s, delimiter = '\n'):
    res = ''
    tmp = ''
    tmpL = 0
    for i in s:
        if(tmpL >= 147988 and i == ' '):
                res += tmp
                res += delimiter
                tmpL = 0
                tmp = ''
        else:
            tmp += i
            tmpL += 1
    if(tmpL >= 1):
        res += tmp
    return res

def reformat_time(s):
    utc_time = datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ")
    return utc_time#.strftime('%d, %B %Y, %I:%M %p')
    #return utc_time.strftime('%A, %d, %B %Y')

def epoch_diff(s):
#    try:
#        s = s._score
#    except Exception:
#        s = s['_score']
#    return 1000 - float(s)
    try:
        s = s.published_at
    except Exception:
        s = s['published_at']
    utc_time = datetime.strptime(s, "%Y-%m-%dT%H:%M:%S.%fZ")
    epoch = datetime(1970,1,1)
    return (utc_time - epoch).total_seconds()

def getcurrenttime():
    st = datetime.now().strftime('%A, %d, %B %Y %I:%M:%S %p')
    return st


import elasticsearch
es = elasticsearch.Elasticsearch()  # use default of localhost, port 9200

def api_base(request):
    context = {
        'status': 403,
        'reason': 'API KEY Required for public access',
        'help': 'Documentation and public access will be added soon'
    }
    return JsonResponse(context)

#@csrf_protect
def api(request, _method, _query):
    if _method == 'u':
        user_id = _query
        #return JsonResponse({'body' : user_id})
        if request.POST.get('userid') != '':
            user_id = request.POST.get('userid')
        csrf_token = request.POST.get('csrfmiddlewaretoken')
        if(user_id != None and csrf_token != None and request.is_ajax()):
            user_id = str(user_id)
            user_id = user_id.replace('https://www.youtube.com/channel/', '')
            user_id = user_id.replace('http://www.youtube.com/channel/', '')
            user_id = user_id.replace('https://youtube.com/channel/', '')
            user_id = user_id.replace('http://youtube.com/channel/', '')
            user_id = user_id.replace('/videos/', '')
            user_id = user_id.replace('/videos', '')
            url = 'http://www.youtube.com/channel/%s' % (user_id)
            try:
                _comments = es.search(index="comments", body={'size': 1000, "query": {"match": {'author_channel_id':user_id}}})
                _comments['body'] = {"sort": [
                        {'published_at': 'desc'}
                    ],"query": {"match": {'author_channel_id':user_id}}}
                comments = []
                for comment in _comments['hits']['hits']:
                    comments.append(comment['_source'])
                    comments[-1]['_score'] = comment['_score']
                author = comments[0]
                author_name = author['author_name']
                author_channel_url = author['author_channel_url']
                profile_image_url = str(author['profile_image_url'])
                profile_image_url = profile_image_url.replace('/s28-', '/s84-')
                #comments = sorted(comments, key = epoch_diff, reverse=True)
                L = len(comments)
                for i in xrange(L):
                    comments[i]['published_at'] = reformat_time(comments[i]['published_at'])
                    title = comments[i]['video_title']
                    if len(title) > 50:
                        title = title[:30] + '...'
                    comments[i]['video_title'] = title
                    comments[i]['published_at'] = timeago.format(comments[i]['published_at'])
                    comments[i]['comment_text'] = reformat_str(str(comments[i]['comment_text'].encode('utf-8')).replace('<br>', '\n'), delimiter='<br>')
                jsonobj = {
                    'author_name': author_name,
                    'author_channel_url': author_channel_url,
                    'author_image_url': profile_image_url,
                    'response': list(comments),
                    'status': 200,
                    'count': len(comments)
                }
                return JsonResponse(jsonobj, safe=False)
            except IndexError:
                jsonobj = {
                    'status' : 404
                }
                return JsonResponse(jsonobj, safe=False)
            except Exception as e:
                jsonobj = {
                    'status': 500,
                    'Exception': str(e)
                }
                return JsonResponse(jsonobj, safe=False)
        else:
            return HttpResponse('Bad Request')
    elif _method == 's':
        search_term = _query
        csrf_token = request.POST.get('csrfmiddlewaretoken')       
        _from = request.POST.get('next_batch') 
        try:
            _from = int(_from)
        except Exception:
            _from = 0
        if request.POST.get('_query'):
            search_term = request.POST.get('_query')
        if(search_term != None and csrf_token != None and request.is_ajax()):
            search_term = search_term.replace('https://www.youtube.com/channel/', '')
            search_term = search_term.replace('http://www.youtube.com/channel/', '')
            search_term = search_term.replace('https://youtube.com/channel/', '')
            search_term = search_term.replace('http://youtube.com/channel/', '')
            search_term = search_term.replace('/videos/', '')
            search_term = search_term.replace('/videos', '')
            try:
                body = {
                    "sort": [
                        {'published_at': 'desc'}
                    ],
                    "query": {
                        "multi_match": {
                            'query': search_term,
                            "operator": "and",
                            'fields': ['author_channel_id', 'author_name', 'comment_text', 'video_title', 'source_channel_title']
                        }
                    },
                    'size': 20,
                    "from" : _from
                }
                _comments = es.search(index="comments", body=body)
                #_comments['id'] = search_term
                #return JsonResponse(_comments)
                comments = []
                for j, comment in enumerate(_comments['hits']['hits']):
                    comments.append(comment['_source'])
                    comments[-1]['_score'] = comment['_score']
                    if j >= 1000:
                        break
                author = comments[0]
                author_name = author['author_name']
                author_channel_url = author['author_channel_url']
                profile_image_url = str(author['profile_image_url'])
                #author['profile_image_url'] = profile_image_url.replace('/s28-', '/s84-')
                #comments = sorted(comments, key = epoch_diff, reverse=True)
                L = len(comments)
                for i in xrange(L):
                    comments[i]['published_at'] = reformat_time(comments[i]['published_at'])
                    # title = comments[i]['video_title']
                    # if len(title) > 50:
                    #     title = title[:30] + '...'
                    # comments[i]['video_title'] = title
                    comments[i]['profile_image_url'] = comments[i]['profile_image_url'].replace('/s28-', '/s54-')
                    comments[i]['published_at'] = timeago.format(comments[i]['published_at'])
                    comments[i]['comment_text'] = reformat_str(str(comments[i]['comment_text'].encode('utf-8')).replace('<br>', '\n'), delimiter='<br>')
                jsonobj = {
                    'author_name': author_name,
                    'author_channel_url': author_channel_url,
                    'author_image_url': profile_image_url,
                    'response': list(comments),
                    'status': 200,
                    'count': len(comments)
                }
                return JsonResponse(jsonobj, safe=False)
            except IndexError:
                jsonobj = {
                    'status' : 404
                }
                return JsonResponse(jsonobj, safe=False)
            except Exception as e:
                jsonobj = {
                    'status': 500,
                    'Exception': str(e)
                }
                return JsonResponse(jsonobj, safe=False)
        else:
            return HttpResponse('Bad Request')
    else:
        return HttpResponse('Bad Request')

@basic_auth_required
def index(request):
    context = {}
    return render(request, 'core/index.html', context)

def search(request, search_text):
    search_text = search_text.replace('https://www.youtube.com/channel/', '')
    search_text = search_text.replace('http://www.youtube.com/channel/', '')
    search_text = search_text.replace('https://youtube.com/channel/', '')
    search_text = search_text.replace('http://youtube.com/channel/', '')
    search_text = search_text.replace('/videos/', '')
    search_text = search_text.replace('/videos', '')
    _from = request.POST.get('from')
    if _from == '' or _from == None:
        _from = request.GET.get('from')
    try:
        _from = int(_from)
    except Exception:
        _from = 0
    if search_text == '':
        context = {
            'status': 407
        }
        return render(request, 'core/search.html', context)
    try:
        body = {
            "sort": [
                        {'published_at': 'desc'}
                    ],
            "query": {
                "multi_match": {
                    'query': '"' + search_text + '"',
                    'fields': ['author_channel_id', 'author_name', 'comment_text', 'video_title', 'source_channel_title']
                }
            },
            'size': 100,
            'from': _from
        }
        #return JsonResponse(body)
        _comments = es.search(index="comments", body=body)
        #return JsonResponse(_comments)
        comments = []
        for comment in _comments['hits']['hits']:
            comments.append(comment['_source'])
            comments[-1]['_score'] = comment['_score']
        #comments = sorted(comments, key = epoch_diff, reverse=True)
        L = len(comments)
        for i in xrange(L):
            comments[i]['published_at'] = reformat_time(comments[i]['published_at'])
            title = comments[i]['video_title']
            comments[i]['video_title'] = title
            if len(comments[i]['source_channel_title']) > 30:
                comments[i]['source_channel_title'] = comments[i]['source_channel_title'][:30] + '...'
            comments[i]['comment_text'] = reformat_str(
                str(comments[i][u'comment_text'].encode('utf-8')), 
                delimiter='\n'
            )
            comments[i]['profile_image_url'] = comments[i]['profile_image_url'].replace('/s28-', '/s54-')
            comments[i]['published_at'] = timeago.format(comments[i]['published_at'])
        if len(comments) == 0:
            context = {
                'status': 404,
                'search_term': search_text
            }
            #return JsonResponse(context)
        else:
            context = {
                'comments': comments,
                'status': 200,
                'search_term': search_text
            }
            #return JsonResponse(context)
        return render(request, 'core/search.html', context)
    except IndexError:
        context = {
            'status': 404
        }
        return render(request, 'core/search.html', context)

def user(request, user_id):
    user_id = str(user_id)
    user_id = user_id.replace('https://www.youtube.com/channel/', '')
    user_id = user_id.replace('http://www.youtube.com/channel/', '')
    user_id = user_id.replace('https://youtube.com/channel/', '')
    user_id = user_id.replace('http://youtube.com/channel/', '')
    user_id = user_id.replace('/videos/', '')
    user_id = user_id.replace('/videos', '')
    url = 'http://www.youtube.com/channel/%s' % (user_id)
    try:
        _comments = es.search(index="comments", body={"sort": [
                        {'published_at': 'desc'}
                    ],'size': 1000, "query": {"match": {'author_channel_id':user_id}}})
        #return JsonResponse(_comments)
        comments = []
        for comment in _comments['hits']['hits']:
            comments.append(comment['_source'])
        author = comments[0]
        author_name = author['author_name']
        author_channel_url = author['author_channel_url']
        profile_image_url = str(author['profile_image_url'])
        profile_image_url = profile_image_url.replace('/s28-', '/s128-')
        #comments = sorted(comments, key = epoch_diff, reverse=True)
        L = len(comments)
        for i in xrange(L):
            comments[i]['published_at'] = reformat_time(comments[i]['published_at'])
            # title = comments[i]['video_title']
            # if len(title) > 30:
            #     title = title[:30] + '...'
            #comments[i]['video_title'] = title
            # if len(comments[i]['source_channel_title']) > 30:
            #     comments[i]['source_channel_title'] = comments[i]['source_channel_title'][:30] + '...'
            comments[i]['comment_text'] = reformat_str(
                str(comments[i][u'comment_text'].encode('utf-8')), 
                delimiter='\n'
            )
            comments[i]['published_at'] = timeago.format(comments[i]['published_at'])
        context = {
            'comments': comments,
            'author_name': author_name,
            'author_channel_url': author_channel_url,
            'profile_image_url': profile_image_url,
            'status': 200
        }
        return render(request, 'core/user.html', context)
    except IndexError:
        context = {
            'status': 404
        }
        return render(request, 'core/user.html', context)