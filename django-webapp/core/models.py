# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Make sure each ForeignKey has `on_delete` set to the desired behavior.
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
from __future__ import unicode_literals

from django.db import models

class Comments(models.Model):
    comment_id = models.CharField(primary_key=False, max_length=255)
    video_id = models.CharField(max_length=255)
    video_title = models.CharField(max_length=512, blank=True, null=True)
    author_name = models.CharField(max_length=255)    
    source_channel_title = models.CharField(max_length=300)
    source_channel_id = models.CharField(max_length=300)
    source_channel_url = models.CharField(max_length=300)
    author_channel_id = models.CharField(max_length=255)
    author_channel_url = models.CharField(max_length=255)
    comment_text = models.TextField()
    like_count = models.IntegerField()
    published_at = models.CharField(max_length=255)
    parent_id = models.CharField(max_length=255, blank=True, null=True)
    profile_image_url = models.CharField(max_length=255)

    class Meta:
        db_table = 'comments'

class ClientDetails(models.Model):
    ip_addr = models.CharField(max_length=255, blank=True, null=True)
    page = models.CharField(max_length=255, blank=True, null=True)
    time_stamp = models.CharField(max_length=255, blank=True, null=True)
    country = models.CharField(max_length=255, blank=True, null=True)
    region = models.CharField(max_length=255, blank=True, null=True)
    region_name = models.CharField(max_length=255, blank=True, null=True)
    city = models.CharField(max_length=255, blank=True, null=True)
    zip = models.CharField(max_length=255, blank=True, null=True)
    latitude = models.FloatField(blank=True, null=True)
    longitude = models.FloatField(blank=True, null=True)
    isp = models.CharField(max_length=255, blank=True, null=True)
    org = models.CharField(max_length=255, blank=True, null=True)
    field_as = models.CharField(db_column='_as', max_length=255, blank=True, null=True)  # Field renamed because it started with '_'.

    class Meta:
        db_table = 'client_details'
