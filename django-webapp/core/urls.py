from django.conf.urls import url, include
from . import views

# url(r'^api/u/(?P<user_id>.*)', views.api, name = 'api'),
urlpatterns = [
    url(r'^passwd/$', views.passwd, name = 'passwd'),
    url(r'^$', views.index, name = 'index'),
    url(r'^u/(?P<user_id>.*)', views.user, name = 'user'),
    url(r'^s/(?P<search_text>.*)', views.search, name = 'search'),
    url(r'^api/(?P<_method>.*)/(?P<_query>.*)', views.api, name = 'api'),
    url(r'^api/', views.api_base, name = 'api')
]
