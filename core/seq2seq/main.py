#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
Seq2Seq base module
'''
# Start from line 308 and then follow DFS
# manner to understand the code structure.

import argparse
import logging
import math
import os
import pickle
import random
import sys
import time

import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch import optim
from torch.autograd import Variable
from torch.nn.utils import clip_grad_norm

import data_iterator
import util
from models import seq2seq_attn, seq2seq_attn_char_cnn
from modules import Loss, Optimizer, Trainer

use_cuda = torch.cuda.is_available()

#The below line is for reproducibility of results, refer:
# https://github.com/pytorch/pytorch/issues/114 

torch.backends.cudnn.deterministic = True

random.seed(3435)
np.random.seed(3435)
torch.manual_seed(3435)
torch.cuda.manual_seed_all(3435)
torch.cuda.manual_seed(3435)

level = logging.INFO
logging.basicConfig(level=level, format='')

def parse_arguments():
    '''
    Parsing arguments
    '''
    logging.info('Parsing arguments')
    parser = argparse.ArgumentParser()
    data = parser.add_argument_group('data sets; model loading and saving')
    data.add_argument('--datasets', type=str, required=True, metavar='PATH', nargs=2,
                        help="parallel training corpus (source and target)")
    data.add_argument('--datasets_valid', type=str, required=True, metavar='PATH', nargs=2,
                        help="parallel validation corpus (source and target)")
    data.add_argument('--language_objects', type=str, metavar='PATH', nargs=2, 
                        default = ['./data/language_object.train.src.pkl', './data/language_object.train.trg.pkl'],
                        help="paths to pickle files for data_iterator.Lang objects (source and target).")
    data.add_argument('--language_objects_valid', type=str, metavar='PATH', nargs=2, 
                        default = ['./data/language_object.valid.src.pkl', './data/language_object.valid.trg.pkl'],
                        help="paths to pickle files for data_iterator.Lang objects (source and target).")
    data.add_argument('--saveTo', type=str, default='./data/model.pt', metavar='PATH', required=True,
                        help="location of model to save (default: %(default)s)")
    data.add_argument('--saveFreq', type=int, default=30000, metavar='INT',
                        help="save frequency (default: %(default)s)")
    data.add_argument('--overwrite', action='store_true',
                        help="write all models to same file")
    data.add_argument('--char', action='store_true',
                        help="use character cnn on words of encoder")
    
    network = parser.add_argument_group('network parameters')
    network.add_argument('--dim_word_src', type=int, default=500, metavar='INT',
                        help="source embedding layer size (default: %(default)s)")
    network.add_argument('--dim_word_trg', type=int, default=500, metavar='INT',
                        help="target embedding layer size (default: %(default)s)")
    network.add_argument('--dim', type=int, default=500, metavar='INT',
                        help="hidden layer size (default: %(default)s)")
    network.add_argument('--bidirectional', action="store_true",
                        help="bidirectional encoder (default: %(default)s)")
    network.add_argument('--enc_depth', type=int, default=1, metavar='INT',
                        help="number of encoder layers (default: %(default)s)")
    network.add_argument('--dec_depth', type=int, default=1, metavar='INT',
                        help="number of decoder layers (default: %(default)s)") #TODO: decoder with layers more than one(not sure how it works)
    network.add_argument('--dropout', type=float, default=0.2, metavar="FLOAT",
                        help="dropout (0: no dropout) (default: %(default)s)") #TODO: Seperate dropout for enc, dec, layers
    network.add_argument('--encoder', type=str, default='gru', choices=['gru', 'lstm'],
                        help='encoder recurrent layer (default: %(default)s)')
    network.add_argument('--decoder', type=str, default='gru', choices=['gru', 'lstm'],
                        help='first decoder recurrent layer (default: %(default)s)')
    
    training = parser.add_argument_group('training parameters')
    training.add_argument('--maxlen', type=int, default=50, metavar='INT',
                        help="maximum target sequence length (default: %(default)s)") #TODO: different lengths for src, trg
    training.add_argument('--optimizer', type=str, default="sgd", 
                        choices=['adam', 'adadelta', 'rmsprop', 'sgd', 'sgdmomentum'],
                        help="optimizer (default: %(default)s)")
    training.add_argument('--lrate', type=float, default=1.0, metavar='FLOAT',
                        help="learning rate (default: %(default)s)")
    training.add_argument('--max_grad_norm', type=float, default=5, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--lrate_decay', type=float, default=0.5, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--start_decay_at', type=float, default=16, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--adam_beta1', type=float, default=0.9, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--adam_beta2', type=float, default=0.999, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--adagrad_accumulator_init', type=float, default=0, metavar='FLOAT',
                        help="TODO (default: %(default)s)")
    training.add_argument('--decay_method', type=str, default="", 
                        choices=['noam'], help="lr decay method (default: %(default)s)")
    training.add_argument('-warmup_steps', type=int, default=4000,
                       help="""Number of warmup steps for custom decay.""")
    training.add_argument('--batch_size', type=int, default=64, metavar='INT',
                        help="minibatch size (default: %(default)s)")
    training.add_argument('--max_epochs', type=int, default=20, metavar='INT',
                        help="maximum number of epochs (default: %(default)s)")
    
    training.add_argument('--norm_method', type=str, default="sent", 
                        choices=['sent', 'word'], help="normalization method (default: %(default)s)")
    training.add_argument('--no_shuffle', action="store_false", dest="shuffle_each_epoch",
                        help="disable shuffling of training data (for each epoch)")
    training.add_argument('--objective', choices=['CE', 'MRT', 'RAML'], default='CE', #TODO: MRT, RAML
                        help='training objective. CE: cross-entropy minimization (default); MRT: Minimum Risk Training (https://www.aclweb.org/anthology/P/P16/P16-1159.pdf) \
                        RAML: Reward Augmented Maximum Likelihood (https://papers.nips.cc/paper/6547-reward-augmented-maximum-likelihood-for-neural-structured-prediction.pdf)')
    
    validation = parser.add_argument_group('validation parameters')
    validation.add_argument('--valid_datasets', type=str, default=None, metavar='PATH', nargs=2,
                        help="parallel validation corpus (source and target) (default: %(default)s)")
    validation.add_argument('--valid_batch_size', type=int, default=80, metavar='INT',
                        help="validation minibatch size (default: %(default)s)")
    validation.add_argument('--validFreq', type=int, default=10000, metavar='INT',
                        help="validation frequency (default: %(default)s)")
    validation.add_argument('--patience', type=int, default=10, metavar='INT',
                        help="early stopping patience (default: %(default)s)")
    
    display = parser.add_argument_group('display parameters')
    display.add_argument('--dispFreq', type=int, default=1, metavar='INT',
                        help="display loss after INT updates (default: %(default)s)")
    display.add_argument('--evaluateFreq', type=int, default=10, metavar='INT',
                        help="evaluate model after INT epochs (default: %(default)s)")
    display.add_argument('--sampleFreq', type=int, default=1000, metavar='INT',
                        help="display some samples after INT updates (default: %(default)s)")
    
    args = parser.parse_args()

    return args

def get_Lang_object(dataset_paths, pkl_file_paths):
    '''
    Loading/creating the data_iterator.Lang objects.
    If newly creating, the pickle files will be saved in ./data/ folder.
    '''
    if os.path.isfile(pkl_file_paths[0]) and os.path.isfile(pkl_file_paths[1]):
        '''
        Loading data_iterator.Lang objects for source and target languages.
        '''
        logging.info('Lang objects found, loading')
        source_data = util.read_pickle(pkl_file_paths[0])
        target_data = util.read_pickle(pkl_file_paths[1])
    else:
        '''
        Creating data_iterator.Lang objects for source and target languages.
        '''
        logging.info('Lang objects not found, creating')
        source_data = data_iterator.Lang(dataset_paths[0], args.maxlen, 'source', chars = args.char)
        target_data = data_iterator.Lang(dataset_paths[1], args.maxlen, 'target')
        logging.info('Saving source lang at %s' % (pkl_file_paths[0]))
        util.write_pickle(source_data, pkl_file_paths[0], pickle.HIGHEST_PROTOCOL)
        logging.info('Saving target lang at %s' % (pkl_file_paths[1]))
        util.write_pickle(target_data, pkl_file_paths[1], pickle.HIGHEST_PROTOCOL)
    return source_data, target_data

def get_data(args):
    '''
    load data_iterator.Lang and build data_iterator.dataIterator objects
    '''
    source_data, target_data = get_Lang_object(args.datasets, args.language_objects)
    source_data_valid, target_data_valid = get_Lang_object(args.datasets_valid, args.language_objects_valid)
    data_iter = data_iterator.dataIterator(source_data, target_data)
    data_iter_valid = data_iterator.dataIterator(source_data_valid, target_data_valid, shuffle = False)
    
    #source_data.saveVocab('./data/src.vocab.en')
    #target_data.saveVocab('./data/tgt.vocab.hi')
    
    return source_data, target_data, source_data_valid, target_data_valid, data_iter, data_iter_valid
    

def get_model(args):
    '''
    Build model
    '''
    #TODO: Make this user friendly.
    logging.info('Building encoder')    

    if not args.char:
        encoder1 = seq2seq_attn.encoder(
            'LSTM',
            bidirectional = True,
            num_layers = args.enc_depth, 
            hidden_size = 500,
            vocab_size = source_data.n_words,
            embedding_dim = 500,
            pad_token = source_data.word2idx['PAD'], 
            dropout = 0.4).cuda()

        # encoder2 = seq2seq_attn.encoder(
        #     'LSTM',
        #     bidirectional = True,
        #     num_layers = args.enc_depth, 
        #     hidden_size = 128,
        #     vocab_size = source_data.n_words,
        #     embedding_dim = 128,
        #     pad_token = source_data.word2idx['PAD'], 
        #     dropout = 0.4).cuda()

        # encoder3 = seq2seq_attn.encoder(
        #     'LSTM',
        #     bidirectional = True,
        #     num_layers = args.enc_depth, 
        #     hidden_size = 256,
        #     vocab_size = source_data.n_words,
        #     embedding_dim = 256,
        #     pad_token = source_data.word2idx['PAD'], 
        #     dropout = args.dropout).cuda()

        # encoder4 = seq2seq_attn.encoder(
        #     'LSTM',
        #     bidirectional = True,
        #     num_layers = args.enc_depth, 
        #     hidden_size = 512,
        #     vocab_size = source_data.n_words,
        #     embedding_dim = 512,
        #     pad_token = source_data.word2idx['PAD'], 
        #     dropout = args.dropout).cuda()
            
    else:
        encoder = seq2seq_attn_char_cnn.encoder(
            'LSTM',
            bidirectional = True,
            num_layers = args.enc_depth, 
            hidden_size = args.dim,
            vocab_size = source_data.n_chars,
            embedding_dim = args.dim_word_src,
            char_embedding_dim = 32,
            pad_token = source_data.char2idx['PAD'], 
            kernel_sizes = [2, 3, 4, 5, 6],
            filters = [128, 128, 128, 256, 256],
            dropout = args.dropout).cuda()
    
    logging.info('Building decoder')    
    decoder = seq2seq_attn.decoder(
        'LSTM', 
        bidirectional_encoder = True,
        num_layers = args.dec_depth,
        hidden_size = 500,
        vocab_size = target_data.n_words,
        embedding_dim = 500,
        pad_token = target_data.word2idx['PAD'],
        attn_type = 'general',
        dropout = 0.4).cuda()
    
    model = seq2seq_attn.Seq2SeqAttention(encoder1, decoder).cuda()
    logging.info('Initializing model parameters')
    #Refer: https://discuss.pytorch.org/t/initializing-embeddings-for-nmt-matters-a-lot/10517
    for p in model.parameters():
        p.data.uniform_(-0.1, 0.1)
    
    #model.encoder[0].embeddings.weight.data[0].fill_(0)
    #model.encoder[0].embeddings.weight.data[1].fill_(0) # padding index
    
    #model.encoder[1].embeddings.weight.data[0].fill_(0)
    #model.encoder[1].embeddings.weight.data[1].fill_(0) # padding index
        
    
    #model.encoder[2].embeddings.weight.data[0].fill_(0)
    #model.encoder[2].embeddings.weight.data[1].fill_(0) # padding index
    
    #model.encoder[3].embeddings.weight.data[0].fill_(0)

    #model.decoder.embeddings.weight.data[1].fill_(0) # padding index

    #print model.encoder[0].embeddings.weight.data[0]
    """
    f = open('/home/chaitanya/Research/datasets/english/word2vec_eng_dim_64.en.pkl')
    emb = pickle.load(f)
    f.close()
    emb = torch.FloatTensor(emb).cuda()
    model.encoder[0].embeddings.weight = nn.Parameter(emb)
    model.encoder[0].embeddings.weight.data[0].fill_(0)

    f = open('/home/chaitanya/Research/datasets/english/word2vec_eng_dim_128.en.pkl')
    emb = pickle.load(f)
    f.close()
    emb = torch.FloatTensor(emb).cuda()
    model.encoder[1].embeddings.weight = nn.Parameter(emb)
    model.encoder[1].embeddings.weight.data[0].fill_(0)
    
    f = open('/home/chaitanya/Research/datasets/english/word2vec_eng_dim_256.en.pkl')
    emb = pickle.load(f)
    f.close()
    emb = torch.FloatTensor(emb).cuda()
    model.encoder[2].embeddings.weight = nn.Parameter(emb)
    model.encoder[2].embeddings.weight.data[0].fill_(0)

    f = open('/home/chaitanya/Research/datasets/english/word2vec_eng_dim_512.en.pkl')
    emb = pickle.load(f)
    f.close()
    emb = torch.FloatTensor(emb).cuda()
    model.encoder[3].embeddings.weight = nn.Parameter(emb)
    model.encoder[3].embeddings.weight.data[0].fill_(0)
    """

    print(model)
    #exit(0)
    return None, None, model

def load_model():
    model = torch.load('./data/model.pt')
    model.eval()
    print model
    return None, None, model

def get_optimizer(args, model):
    '''
    Build optimizer
    '''
    optimizer = Optimizer.Optimizer(
        args.optimizer,
        args.lrate,
        args.max_grad_norm,
        args.lrate_decay,
        args.start_decay_at,
        args.adam_beta1,
        args.adam_beta2,
        args.adagrad_accumulator_init,
        args.decay_method,
        args.warmup_steps,
        args.dim
    )
    optimizer.set_parameters(model.parameters())
    return optimizer

def get_criterion(args, target_data):
    '''
    Build error criterion.
    Only supports NLLLoss()
    '''
    criterion = Loss.NMTLoss(target_data.word2idx, target_data.word2idx['PAD'])
    return criterion.cuda()

def train_model(args, model, train_criterion, valid_criterion, optimizer,
                source_data, target_data, source_data_valid, target_data_valid, 
                train_data_iterator, valid_data_iterator):
    
    logging.info('Optimizing')        
    
    #normalization method: whether to normalize the loss per sentences or per words.
    norm_method = args.norm_method

    #Instantiate the trainer module. 
    prev = 100000000000000000000
    trainer = Trainer.Trainer(model, train_criterion, valid_criterion, optimizer, norm_method=norm_method)

    for epoch in xrange(1, args.max_epochs + 1):
        # Train the model on training set
        train_data_iterator.reset()
        train_stats = trainer.train(args, train_data_iterator, source_data, target_data, epoch)
        msg = ('=' * 80) + '\nEpoch: %d, Train Loss: %.3f, Train Accuracy: %.3f, Train Perplexity: %.3f'
        msg = msg % (epoch, train_stats._loss(), train_stats.accuracy(), train_stats.perplexity())
        logging.info(msg)

        # Validate the model on validation set
        valid_data_iterator.reset()
        valid_stats = trainer.validate(args, valid_data_iterator, source_data, target_data)
        msg = 'Epoch: %d, Valid Loss: %.3f, Valid Accuracy: %.3f, Valid Perplexity: %.3f\n' + ('=' * 80)
        msg = msg % (epoch,valid_stats._loss(),valid_stats.accuracy(),valid_stats.perplexity())

        trainer.epoch_step(valid_stats.perplexity(), epoch)

        #TODO:
        #See https://github.com/OpenNMT/OpenNMT-py/blob/master/onmt/Trainer.py#L226
        #Implement drop_checkpoint()
        #if epoch >= opt.start_checkpoint_at:
        #    trainer.drop_checkpoint(model_opt, epoch, fields, valid_stats)
        
        logging.info(msg)
        #exit(0)        
        #if(valid_stats.perplexity() < prev):
        if(True):
            logging.info('Saving model')            
            torch.save(model, args.saveTo)
        torch.save(model, './data/model_epoch_%d_valid_ppl_%.4f_.pt' % (epoch, valid_stats.perplexity()))
        prev = min(prev, valid_stats.perplexity())

if __name__ == '__main__':
    #parse command line arguments
    args = parse_arguments()

    #Load data
    source_data, target_data, source_data_valid, target_data_valid, train_data_iterator, valid_data_iterator = get_data(args)
    #Build model
    #for i in x:
    #    print 'actual:', i
    #    source_data.sentence2Sequence(i.strip(), maxlen = len(i.split()))

    #exit(0)
    encoder, decoder, model = get_model(args)
    #encoder, decoder, model = load_model()

    #Build criterion
    train_criterion = get_criterion(args, target_data)
    valid_criterion = get_criterion(args, target_data)

    #Build optimizer
    optimizer = get_optimizer(args, model)

    #Train model
    train_model(args, model, train_criterion, valid_criterion, optimizer,
                source_data, target_data, source_data_valid, target_data_valid, 
                train_data_iterator, valid_data_iterator)
