#!/usr/bin/env python
# -*- coding: utf-8 -*-

'''
General utils
'''

import pickle
import subprocess

def fread(filename, mode='r'):
    f = open(filename)
    return open(filename, mode).read().strip()

def write_pickle(obj, filename, *args):
    f = open(filename, 'wb')
    pickle.dump(obj, f, *args)

def read_pickle(filename):
    f = open(filename, 'rb')
    return pickle.load(f)

def write_file(log, filename, mode = 'a+'):
    f = open(filename, mode)
    f.write(log + '\n')
    f.close()
