## Crawling comments and storing them using elastic search.

- Used few keywords and youtube search to fetch all the related channels (Similar to Google web spiders).
- Used youtube-dl to fetch video urls from a particular channel (Youtube api is costly for this operation).
- Used Youtube api v3 to fetch comments from a particular video.
