import argparse
import codecs
import csv
import datetime
import json
import logging
import os
import sys
import threading
import time
from urllib.parse import quote
from urllib.request import urlopen  # python3

import elasticsearch
import lxml.html
import MySQLdb
import requests
from bs4 import BeautifulSoup
from elasticsearch.helpers import bulk
from lxml.cssselect import CSSSelector
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options

import youtube_dl
from config import *


def readFile(fname, split_by = None):
    try:
        with open(fname) as f:
            x = f.read()
            x = x.strip()
            if(split_by != None):
                x = x.split(split_by)
        return x
    except IOError:
        x = ''
        if(split_by != None):
            return []
        return ''

API_KEYS = readFile(API_KEY_FILE, '\n')
os.environ['API_KEY'] = API_KEYS[5]
os.environ['API_KEY_IDX'] = str(5)
API_KEY = os.environ['API_KEY']

last_updated_cache = {}

comment_write_lock = threading.Lock()
std_log_lock = threading.Lock()
api_key_lock = threading.Lock()

es = elasticsearch.Elasticsearch(timeout = 3600)

tracer = logging.getLogger('elasticsearch')
tracer.setLevel(logging.CRITICAL)
os.environ['NO_PROXY'] = 'localhost;10.1.34.164'

def loadUrl(url, parse_json = True, depth = 0, use_proxy = True):
    if use_proxy == False:
        proxies = {
            "http": None,
            "https": None,
        }
    else:
        proxies = {
            'http': 'http://proxy.iiit.ac.in:8080',
            'https': 'http://proxy.iiit.ac.in:8080'
        }
    try:
        if(parse_json):
            response = requests.get(url, proxies=proxies).json()
        else:
            response = requests.get(url, proxies = proxies)
            return (True, response)
        #print url
        #response = str(urlopen(url).read())
        return (True, response)
    except KeyError:
        return (False, 'Error')
    except Exception as e:
        if(depth == 10):
            sys.stderr.write('Exception occurred in loadurl: %s, Ending as depth >= 10\n' % str(e))
            sys.stderr.flush()
            return (False, 'Error')
        else:
            sys.stderr.write('Exception occurred in loadurl: %s, retrying\n' % str(e))
            sys.stderr.flush()
            time.sleep(20)
            return loadUrl(url, parse_json, depth + 1, use_proxy)
    #except Exception as e:
    #    return (False, e)

class ThreadSafeLogWriter(object):
    def __init__(self):
        self.messages = []
        self.counter = 0
        for i in range(N_THREADS + 1):
            self.messages.append('Thread id: %d, msg: ' % (i))
        self.lock = threading.Lock()
        
    def write(self, msg, thread_id):
        self.lock.acquire()
        self.messages[thread_id] = 'Thread id: %d, msg: %s' % (thread_id, msg)
        self.counter += 1
        self.counter %= 64#N_THREADS # There is no reason for selecting N_THREADS; Any number > 0 is fine.
        if(self.counter == 0):
            sys.stderr.flush()
            print("\033[H\033[J")
            s = ''
            for i in self.messages:
                s += i
                s += '\n'
                #print(i)
            #print('Using API-KEY IDX: %s' % (os.environ['API_KEY_IDX']))
            s += 'Using API-KEY IDX: %s' % (os.environ['API_KEY_IDX'])
            print(s)
        self.lock.release()

class ytdlLogger(object):
    def __init__(self, thread_id, lw, channel_name):
        self.thread_id = thread_id
        self.channel_name = channel_name
        self.lw = lw
    
    def debug(self, msg):
        # msg = self.channel_name
        # self.lw.write(msg, self.thread_id)
        # return
        # if 'download' in msg:
        #    msg = self.channel_name + ' | ' + msg
        #    self.lw.write(msg, self.thread_id)
        # time.sleep(0.5)
        return
    
    def error(self, msg):
        sys.stderr.write('%s\n' % msg)
    
    def warning(self, msg):
        sys.stderr.write('%s\n' % msg)

def getIndexCountES(index):
    res = loadUrl('http://10.1.34.164:9200/%s/_count' % index, parse_json = True, use_proxy = False)
    try:
        tmp = res[1]['count']
    except KeyError:
        tmp = 1
    return tmp

logwriter = ThreadSafeLogWriter()
last_committed = 1
comment_id = getIndexCountES('idx2comment')
ACTIONS = []
ACTIONS1 = []

class YouTubeComment(object):
    def __init__(self, video_id, comment_id, author_name, author_channel_id, 
                author_channel_url, author_channel_profile_image, 
                comment_text, like_count, published_at, parent_id = None):
        self.video_id = video_id
        self.comment_id = comment_id
        self.author_name = author_name
        self.author_channel_id = author_channel_id
        self.author_channel_url = author_channel_url
        self.author_channel_profile_image = author_channel_profile_image
        self.comment_text = comment_text
        self.like_count = like_count
        self.published_at = published_at
        self.parent_id = parent_id
        self.replies = []
        if self.parent_id == '':
            self.parent_id = None
        self.query = [buildInsertCmd('comments', 15)]
        self.row = [u'id', u'comment_id', u'video_id', u'video_title', u'source_channel_title', u'source_channel_id', u'source_channel_url', u'author_name', u'author_channel_id', u'author_channel_url', u'comment_text', u'like_count', u'published_at', u'parent_id', u'profile_image_url', u'replies']
        self.rowL = len(self.row)

        # for i in range(8):
        #     self.query.append(buildInsertCmd('comments%s' % (i + 1), 12))
        # self.query1 = buildInsertCmd('comments1', 12)
        # self.query2 = buildInsertCmd('comments2', 12)
        # self.query3 = buildInsertCmd('comments3', 12)
        # self.query4 = buildInsertCmd('comments4', 12)
        # self.query5 = buildInsertCmd('comments5', 12)
        # self.query6 = buildInsertCmd('comments6', 12)
        # self.query7 = buildInsertCmd('comments7', 12)
        # self.query8 = buildInsertCmd('comments8', 12)
    
    def write_to_file(self, fname):
        '''
        writes to csv file
        '''
        comment_write_lock.acquire()
        f = csv.writer(open(fname, 'a+'))
        towrite = [self.comment_id, self.video_id, self.author_name, self.author_channel_id, self.author_channel_url, self.comment_text, self.like_count, self.published_at, self.parent_id, self.author_channel_profile_image.replace('\r', '').replace('\0', '')]
        f.writerow(towrite)
        comment_write_lock.release()
        
    def write_to_db(self, cursor, conn, video_title, thread_id, source_channel_title, source_channel_id, source_channel_url):
        comment_write_lock.acquire()    
        global last_committed    
        towrite = [last_committed, self.comment_id.replace('\r', '').replace('\0', ''), self.video_id.replace('\r', '').replace('\0', ''), video_title.replace('\r', '').replace('\0', ''), source_channel_title, source_channel_id, source_channel_url, self.author_name.replace('\r', '').replace('\0', ''), self.author_channel_id.replace('\r', '').replace('\0', ''), self.author_channel_url.replace('\r', '').replace('\0', ''), self.comment_text.replace('\r', '').replace('\0', ''), str(self.like_count).replace('\r', '').replace('\0', ''), self.published_at.replace('\r', '').replace('\0', ''), self.parent_id, self.author_channel_profile_image.replace('\r', '').replace('\0', '')]
        #print(self.query)
        #print(towrite)
        try:
            cursor[0].execute(self.query[0], towrite)
        except Exception as e:
            sys.stderr.write('Exception while writing: %s\n' % str(e))
        last_committed += 1
        last_committed %= 10000
        if(last_committed == 0):
            sys.stderr.write('    Committing\n')
            conn.commit()
        comment_write_lock.release()
    
    def write_to_elastic_search(self, cursor, conn, video_title, thread_id, source_channel_title, source_channel_id, source_channel_url):
        global last_committed    
        global ACTIONS
        global ACTIONS1
        global comment_id
        logwriter.write('At 151', thread_id)
        towrite = [comment_id, self.comment_id.replace('\r', '').replace('\0', ''), self.video_id.replace('\r', '').replace('\0', ''), video_title.replace('\r', '').replace('\0', ''), source_channel_title, source_channel_id, source_channel_url, self.author_name.replace('\r', '').replace('\0', ''), self.author_channel_id.replace('\r', '').replace('\0', ''), self.author_channel_url.replace('\r', '').replace('\0', ''), self.comment_text.replace('\r', '').replace('\0', ''), str(self.like_count).replace('\r', '').replace('\0', ''), self.published_at.replace('\r', '').replace('\0', ''), self.parent_id, self.author_channel_profile_image.replace('\r', '').replace('\0', ''), self.replies]
        try:
            _body = {}
            for k in range(self.rowL):
                _body[self.row[k]] = towrite[k]
            action = {
                '_index': 'comments',
                '_type': 'comments',
                '_id': self.comment_id,
                '_source': _body
            }
            _body = {
                'comment_id' : self.comment_id
            }
            action1 = {
                '_index': 'idx2comment',
                '_type': 'idx2comment',
                '_id': comment_id,
                '_source': _body
            }
            logwriter.write('At 163, Acquiring lock', thread_id)
            comment_write_lock.acquire()    
            ACTIONS.append(action)
            ACTIONS1.append(action1)
            comment_write_lock.release()
            #es.index(index = 'comments', doc_type = 'comments', id = towrite[0], body = body)
        except Exception as e:
            sys.stderr.write('Exception while writing: %s\n' % str(e))
            return
        logwriter.write('At 171, Acquiring lock', thread_id)
        comment_write_lock.acquire()            
        last_committed += 1
        last_committed %= 10000
        comment_id += 1
        if(last_committed == 0):
            sys.stderr.write('    Committing\n')
            logwriter.write('At 178, Pushing bulk', thread_id)
            success, _ = bulk(es, ACTIONS, index = 'comments', raise_on_error = True)
            success, _ = bulk(es, ACTIONS1, index = 'idx2comment', raise_on_error = True)
            #sys.stderr.write('        %s | %s\n' % (str(success), str(_)))
            ACTIONS = []
            ACTIONS1 = []
        comment_write_lock.release()
        
    def _print(self):
        print("==============")
        print(self.video_id)
        print(self.author_channel_id)
        print(self.author_name)
        print(self.comment_text)
        print(self.like_count)
        print("==============")        
        

def clean_unwanted(link):
    link = link.replace('https://www.youtube.com/user/', '')
    res = ''
    for i in link:
        if(i == '/'):
            break
        res += i
    return 'https://www.youtube.com/user/%s' % res

def buildInsertCmd(table, numfields):

    """
    Create a query string with the given table name and the right
    number of format placeholders.

    example:
    >>> buildInsertCmd("foo", 3)
    'insert into foo values (%s, %s, %s)' 
    """
    assert(numfields > 0)
    placeholders = (numfields-1) * "%s, " + "%s"
    query = ("insert into %s" % table) + (" values (%s)" % placeholders)
    return query

def update_api_key(key, thread_id):
    logwriter.write('At 219, Acquiring lock', thread_id)
    api_key_lock.acquire()
    if(os.environ['API_KEY'] == key):
        API_KEYS = readFile(API_KEY_FILE, '\n')
        idx = int(os.environ['API_KEY_IDX'])
        idx += 1
        idx %= len(API_KEYS)
        os.environ['API_KEY'] = API_KEYS[idx]
        os.environ['API_KEY_IDX'] = str(idx)
    else:
        pass
    logwriter.write('At 230, Releasing lock', thread_id)
    api_key_lock.release()
    return os.environ['API_KEY']


def isDailyLimitExceeded(response):
    try:
        reason = response['error']['errors'][0]['reason']
        if(reason == 'dailyLimitExceeded' or reason == 'quotaExceeded'):
            return True
    except KeyError:
        pass
    return False

def isCommentsDisabled(response):
    try:
        reason = response['error']['errors'][0]['reason']
        if(reason == 'commentsDisabled'):
            return True
    except KeyError:
        pass
    return False

def errorCode(response):
    try:
        ec = int(response['error']['code'])
        return ec
    except Exception:
        return -1

def get_last_updated(last_updated_file):
    # arg: last_updated_file is just video id.
    try:
        response = es.get(
            index = 'videos',
            doc_type = 'videos',
            id = last_updated_file
        )
        if response['found']:
            last_updated = float(response['_source']['last_processed'])
        else:
            last_updated = datetime.datetime(2018,1,1,0,0).timestamp()
        return response['found']
    except Exception as e:
        last_updated = datetime.datetime(2018,1,1,0,0).timestamp()
    return last_updated

def too_old_video(video_id, published_at):
    utc_time = datetime.datetime.strptime(published_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    time_stamp = utc_time.timestamp()
    limit = datetime.datetime(2018,1,1,0,0).timestamp() #July-1-2017
    if time_stamp > limit:
        return False
    return True

def too_early(video_id, published_at):
    utc_time = datetime.datetime.strptime(published_at, "%Y-%m-%dT%H:%M:%S.%fZ")
    time_stamp = utc_time.timestamp()
    limit = get_last_updated(video_id)
    if(time_stamp > limit):
        return False
    return True

def isRelatedVideo(video_id, thread_id):
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=%s' % (video_id, API_KEY)
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response)):
            logwriter.write('At 338, Daily limit exceeded, sleeping', thread_id)
            time.sleep(10)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet&id=%s&key=%s' % (video_id, API_KEY)
            status, response = loadUrl(URL)
        else:
            break
        checked += 1
    if status == False:
        return 0
    try:
        published_at = response['items'][0]['snippet']['publishedAt']
        res = too_old_video(video_id, published_at)
        if res == False:
            return True
        return False
        # if too_old_video(video_id, published_at):
        #    return False
        # return True
        # title = response['items'][0]['snippet']['title']
        # title = title.lower()
        # description = response['items'][0]['snippet']['description']
        # description = description.lower()
        # for i in ['telugu', 'friends', 'andhra', 'telangana']:
        #     if i in title.strip().split():
        #         return 2
        #     if i in description.strip().split():
        #         sys.stderr.write('i : %s | title: %s\n' % (i, str(description)))                
        #         return 2
        # for i in REQUIRED_KEY_WORDS:
        #     if i in title.strip().split():
        #         sys.stderr.write('i : %s | title: %s\n' % (i, str(title)))
        #         return 1
        #     if i in description.strip().split():
        #         sys.stderr.write('i : %s | title: %s\n' % (i, str(description)))                
        #         return 1
    except KeyError:
        pass
    return 0

#FIXME: It's returning True always
def isRelatedChannel(all_videos, channel_title, thread_id):
    return True
    if(len(all_videos) <= 200):
        return True
    for i in REQUIRED_KEY_WORDS:
        if i in channel_title.lower().strip().split():
            return True
    ok_count = 0
    for j, video_id in enumerate(all_videos):
        logwriter.write('%s | Checking: %d | Found: %d ' % (channel_title, j, ok_count), thread_id)
        ret = isRelatedVideo(video_id, thread_id)
        if ret >= 1:
            ok_count += 1
        if(ret == 2):
            return True
        if ok_count >= 100:
            return True
        if j >= 300:
            return False
    return False

def username_to_channel_id(username, thread_id):
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/channels?key=%s&forUsername=%s&part=id' % (API_KEY, username)
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response)):
            logwriter.write('At 402, Daily limit exceeded, sleeping', thread_id)
            time.sleep(10)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/channels?key=%s&forUsername=%s&part=id' % (API_KEY, username)
            status, response = loadUrl(URL)
        else:
            break
        checked += 1
    if(status == False):
        sys.stderr.write('Error: %s\n' % response)
        return None
    return response['items'][0]['id']


def get_video_title(video_id, thread_id):
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/videos?key=%s&id=%s&part=snippet' % (API_KEY, video_id)
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response)):
            logwriter.write('At 423, Daily limit exceeded, sleeping', thread_id)
            time.sleep(10)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/videos?key=%s&id=%s&part=snippet' % (API_KEY, video_id)
            status, response = loadUrl(URL)
        else:
            break
        checked += 1
    try:
        title = response['items'][0]['snippet']['title']
    except Exception:
        return 'Exception | %s' % (response)
    return title

def get_channel_title(channel, thread_id):
    channel = channel.replace('https://www.youtube.com/channel/', '')
    channel = channel.replace('https://youtube.com/channel/', '')
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/channels?key=%s&id=%s&part=snippet' % (API_KEY, channel)
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response)):
            logwriter.write('At 446, Daily limit exceeded, sleeping', thread_id)
            time.sleep(10)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/channels?key=%s&id=%s&part=snippet' % (API_KEY, channel)
            status, response = loadUrl(URL)
        else:
            break
        checked += 1
    try:
        title = response['items'][0]['snippet']['title']
    except Exception:
        return 'Exception | %s' % (response)
    return title

def get_channel_id(channel, thread_id):
    API_KEY = os.environ['API_KEY']
    if('https://www.youtube.com/channel/' in channel):
        channel = channel.replace('https://www.youtube.com/channel/', '')
        channel = channel.rstrip().strip('/')
        return channel
    else:
        assert('https://www.youtube.com/user/' in channel)
        channel = channel.replace('https://www.youtube.com/user/', '')
        channel = channel.rstrip().strip('/')
        return username_to_channel_id(channel, thread_id)

def get_all_channel_videos(url, thread_id):
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    
    driver = webdriver.Firefox(firefox_options = chrome_options)
    driver.get(url)
    SCROLL_PAUSE_TIME = 0.5
    it = 0
    while it <= 200:
        logwriter.write('At: %s' % it, thread_id)
        driver.execute_script("var scrollingElement = (document.scrollingElement || document.body);scrollingElement.scrollTop = scrollingElement.scrollHeight;")
        time.sleep(SCROLL_PAUSE_TIME)
        it += 1
    elems = driver.find_elements_by_xpath("//a[@href]")
    res = []
    prev = None
    for elem in elems:
        link = elem.get_attribute("href")
        if('/watch?v=' in link and link != prev):
            prev = link
            link = link.replace('https://www.youtube.com/watch?v=', '')
            link = link.replace('http://www.youtube.com/watch?v=', '')
            link = link.replace('www.youtube.com/watch?v=', '')
            link = link.replace('youtube.com/watch?v=', '')
            res.append(link)
    driver.quit()
    return res

def _get_videos_by_channel_id(channel_id, page_token, all_videos, _patience, depth = 0, thread_id = -1):
    API_KEY = os.environ['API_KEY']
    #Few issues with youtube API. Look at get_all_channel_videos()
    URL = 'https://www.googleapis.com/youtube/v3/search?key=%s&channelId=%s&part=snippet,id&order=date&maxResults=50&pageToken=%s' % (API_KEY, channel_id, page_token)
    #print URL
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response) or errorCode(response) == 400):
            logwriter.write('At 509, Sleeping', thread_id)
            time.sleep(10)
            time.sleep(1)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/search?key=%s&channelId=%s&part=snippet,id&order=date&maxResults=50&pageToken=%s' % (API_KEY, channel_id, page_token)
            status, response = loadUrl(URL)
            if(errorCode(response) == 400):
                return []
        else:
            break
        checked += 1
    if(status == False):
        sys.stderr.write('Error: %s\n' % response)
        return []
    res = []
    for video in response[u'items']:
        try:
            video_id = video[u'id'][u'videoId']
        except KeyError:
            continue
            #exit(0)
        #logwriter.write('Adding video id: %s, depth: %d' % (video_id, depth), thread_id)
        depth += 1
        if(video_id in all_videos):
            _patience += 1
        else:
            _patience = 0
        if(_patience == PATIENCE or depth >= 2000):
            print('patience at : ', _patience)
            print('depth at: ', depth)
            return res
        else:
            res.append(video_id)
    try:
        nextPageToken = response[u'nextPageToken']
        #print nextPageToken
        res = res + _get_videos_by_channel_id(channel_id, nextPageToken, all_videos, _patience, depth, thread_id=thread_id)
    except KeyError as e:
        nextPageToken = None
    return res

def _get_replies_of_a_comment(video_id, parent_id,  page_token, thread_id):
    #FIXME: For some reasons, this fetching is too slow.
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/comments?parentId=%s&maxResults=100&part=snippet&key=%s&pageToken=%s' % (parent_id, API_KEY, page_token)
    
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response) or errorCode(response) == 400):
            logwriter.write('At 559, Sleeping', thread_id)
            time.sleep(10)
            time.sleep(1)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/comments?parentId=%s&maxResults=100&part=snippet&key=%s&pageToken=%s' % (parent_id, API_KEY, page_token)
            status, response = loadUrl(URL)
            if(errorCode(response) == 400):
                return []
        else:
            break
        checked += 1
    replies = []
    try:
        tmp = response['items']
    except KeyError:
        sys.stderr.write("=========Error in line 436 ===========\n")
        sys.stderr.write("%s\n" % str(response))
        sys.stderr.write("====================\n")
        return []
    for comment in response['items']:
        try:
            comment_id = comment['id']
            author_name = comment['snippet']['authorDisplayName']
            author_channel_id = comment['snippet']['authorChannelId']['value']
            author_channel_url = comment['snippet']['authorChannelUrl']
            author_channel_profile_image = comment['snippet']['authorProfileImageUrl']
            comment_text = comment['snippet']['textOriginal']
            like_count = comment['snippet']['likeCount']
            published_at = comment['snippet']['publishedAt']

            cur = YouTubeComment(video_id, comment_id, author_name, author_channel_id, 
                            author_channel_url, author_channel_profile_image, 
                            comment_text, like_count, published_at, parent_id)
            replies.append(cur)
        except KeyError:
            continue
    try:
        nextPageToken = response['nextPageToken']
        if(nextPageToken != page_token):
            replies = replies + _get_replies_of_a_comment(video_id, parent_id,  nextPageToken, thread_id)
    except KeyError:
        nextPageToken = None
    return replies

def _get_comments_by_video_id(video_id, page_token, all_comment_ids, _patience, inqueue = [], thread_id = -1, sofar = 0, channel_title=''):
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/commentThreads?key=%s&textFormat=plainText&order=time&part=snippet,replies&videoId=%s&maxResults=100&pageToken=%s' % (API_KEY, video_id, page_token)
    status, response = loadUrl(URL)
    if status == False:
        return []
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response) or errorCode(response) == 400):
            logwriter.write('At 612, Sleeping', thread_id)
            time.sleep(10)
            time.sleep(1)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/commentThreads?key=%s&textFormat=plainText&order=time&part=snippet,replies&videoId=%s&maxResults=100&pageToken=%s' % (API_KEY, video_id, page_token)
            status, response = loadUrl(URL)
            if(errorCode(response) == 400):
                return []
            elif(isCommentsDisabled(response)):
                return []
            elif(errorCode(response) in [404, 403]):
                return []
        else:
            break
        checked += 1
    
    if(errorCode(response) == 400):
        return []
    elif(isCommentsDisabled(response)):
        return []
    elif(errorCode(response) in [404, 403]):
        return []
    html = response
    comments = []
    #logging.info('   Parsing video: %s, page_token: %s' % (video_id, page_token))
    #exit(0)
    try:
        tmp = html['items']
    except KeyError:
        sys.stderr.write("=========Error in line 525 ===========\n")
        sys.stderr.write("%s\n" % str(response))
        sys.stderr.write("====================\n")
        return []
    for comment in html['items']:
        parent_id = None
        try:
            cc = comment['snippet']['topLevelComment']
            comment_id = cc['id']
            parent_id = cc['id']
            author_name = cc['snippet']['authorDisplayName']
            author_channel_id = cc['snippet']['authorChannelId']['value']
            author_channel_url = cc['snippet']['authorChannelUrl']
            author_channel_profile_image = cc['snippet']['authorProfileImageUrl']
            comment_text = cc['snippet']['textOriginal']
            like_count = cc['snippet']['likeCount']
            published_at = cc['snippet']['publishedAt']
            try:
                replies_to_current_comment_json = comment['replies']['comments']
            except KeyError:
                replies_to_current_comment_json = []
            #sys.stderr.write('%s\n' % (str(too_early(published_at))))
            #sys.stderr.flush()
            #print(sofar)
            if(too_early(video_id, published_at)):
                return comments
            cur = YouTubeComment(video_id, comment_id, author_name, author_channel_id, 
                            author_channel_url, author_channel_profile_image, 
                            comment_text, like_count, published_at)
        except KeyError as e:
            continue
            #logwriter.write('\n=============\nKeyError: %s | URL: %s\n comment_id: %s\n=============\n' % (e, URL, comment_id), thread_id)
            #logwriter.write('comment_id')
            
        if(comment_id in all_comment_ids):
            _patience += 1
        else:
            _patience = 0
        if(_patience == PATIENCE):
            return comments
        else:
            if _patience == 0:
                comments.append(cur)
                sofar += 1
                logwriter.write('%s | %d/%d videos done | %d comments done' % (channel_title[0], channel_title[1], channel_title[2], sofar), thread_id)    
        if(_patience == 0 and comment['snippet']['totalReplyCount'] >= REPLIES_COUNT_THRESHOLD):
            replies = _get_replies_of_a_comment(video_id, comment_id,  '', thread_id)    
        else:
            replies = []
            for comment in replies_to_current_comment_json:
                comment_id = comment['id']
                author_name = comment['snippet']['authorDisplayName']
                author_channel_id = comment['snippet']['authorChannelId']['value']
                author_channel_url = comment['snippet']['authorChannelUrl']
                author_channel_profile_image = comment['snippet']['authorProfileImageUrl']
                comment_text = comment['snippet']['textOriginal']
                like_count = comment['snippet']['likeCount']
                published_at = comment['snippet']['publishedAt']

                _cur = YouTubeComment(video_id, comment_id, author_name, author_channel_id, 
                        author_channel_url, author_channel_profile_image, 
                        comment_text, like_count, published_at, parent_id)
                replies.append(_cur)
                cur.replies.append(comment_id)
            # for rep in replies:
            #     cur.replies.append(rep.comment_id)
        comments += replies
        #TODO: 
        # 2) Add video title.
    if(sofar >= COMMENTS_LIMIT):
        sys.stderr.write('TOUCHED COMMENT LIMIT')
        return comments
    try:
        nextPageToken = html['nextPageToken']
        if(nextPageToken != page_token):
            comments = comments + _get_comments_by_video_id(video_id, nextPageToken, all_comment_ids, _patience, inqueue, thread_id, sofar, channel_title)
    except KeyError:
        nextPageToken = None
    return comments

def add_new_video(video_title, video_id, thread_id, channel_title, cursor, conn, source_channel_title, source_channel_id, source_channel_url):
    API_KEY = os.environ['API_KEY']
    #all_comment_ids = readFile(ALL_COMMENTS_IDS_FILE, split_by='\n')
    #FIXME: uncomment above and remove bottom line
    all_comment_ids = []
    all_comments = _get_comments_by_video_id(video_id, '', all_comment_ids, 0, inqueue=[], thread_id = thread_id, channel_title= channel_title)
    #all_comments = _get_comments_by_video_id_non_api(video_id, thread_id)
    try:
        sofar_utc_time = last_updated_cache[video_id]# readFile(video_id)
    except KeyError:
        sofar_utc_time = ''
    try:
        sofar_utc_time = float(sofar_utc_time)
    except ValueError:
        sofar_utc_time = 0
    curL = 0
    for comment in all_comments:
        #comment.write_to_file(cursor)
        #comment.write_to_db(cursor, conn, video_title, thread_id, source_channel_title, source_channel_id, source_channel_url)
        comment.write_to_elastic_search(cursor, conn, video_title, thread_id, source_channel_title, source_channel_id, source_channel_url)
        
        published_at = comment.published_at
        utc_time = datetime.datetime.strptime(published_at, "%Y-%m-%dT%H:%M:%S.%fZ")
        utc_time = utc_time.timestamp()
        sofar_utc_time = max(sofar_utc_time, utc_time)
        curL += 1
    last_updated_cache[video_id] = str(sofar_utc_time)
    body = {
        'id': video_id,
        'last_processed': str(sofar_utc_time)
    }
    es.index(index = 'videos', doc_type = 'videos', id = video_id, body = body)
    #sys.stderr.write('   Video: %s, %s\n' % (video_id, body))
    #sys.stderr.flush()
    return curL

def _fetch_trending_channels(page_token, thread_id):
    API_KEY = os.environ['API_KEY']
    URL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&chart=mostPopular&maxResults=50&regionCode=IN&key=%s&pageToken=%s' % (API_KEY, page_token)
    status, response = loadUrl(URL)
    checked = 0
    while checked <= 100000000:
        if(isDailyLimitExceeded(response)):
            time.sleep(10)
            API_KEY = update_api_key(API_KEY, thread_id)
            URL = 'https://www.googleapis.com/youtube/v3/videos?part=snippet,contentDetails,statistics&chart=mostPopular&maxResults=50&regionCode=IN&key=%s&pageToken=%s' % (API_KEY, page_token)
            status, response = loadUrl(URL)
        else:
            break
        checked += 1
    if(status == False):
        sys.stderr.write('Error: %s\n' % response)
        return []
    all_channels = []
    for video in response['items']:
        channel_id = video['snippet']['channelId']
        all_channels.append(channel_id)
    try:
        nextPageToken = response['nextPageToken']
        if(nextPageToken != page_token):
            all_channels = all_channels + _fetch_trending_channels(nextPageToken, thread_id)
    except KeyError:
        nextPageToken = None
    return all_channels

def _add_channels_from_search(search_token, thread_id):
    API_KEY = os.environ['API_KEY']
    search_token = search_token.replace(' ', '+')
    url = 'https://www.youtube.com/results?sp=EgIQAg%253D%253D&search_query=' + search_token
    chrome_options = Options()
    chrome_options.add_argument("--headless")
    driver = webdriver.Chrome(chrome_options = chrome_options)
    driver.get(url)
    SCROLL_PAUSE_TIME = 1
    it = 0
    print(url)
    while it <= 5:
        sys.stdout.write(' At: %d/5   \r' % (it + 1))
        #print 'url:', url, ' it: ', it
        driver.execute_script("var scrollingElement = (document.scrollingElement || document.body);scrollingElement.scrollTop = scrollingElement.scrollHeight;")
        time.sleep(SCROLL_PAUSE_TIME)
        it += 1
    sys.stderr.write('\n')
    elems = driver.find_elements_by_xpath("//a[@href]")
    res = []
    prev = None
    for elem in elems:
        link = elem.get_attribute("href")
        if('https://www.youtube.com/channel/' in link and link != prev):
            prev = link
            res.append(link)
        elif('https://www.youtube.com/user/' in link and link != prev):
            channel_link = get_channel_id(link, thread_id)
            res.append('https://www.youtube.com/channel/%s' % channel_link)
    driver.quit()
    return res


def _add_channels_from_search_non_selenium(search_token, ch = True, end = 10):
    API_KEY = os.environ['API_KEY']
    #search_token = search_token.replace(' ', '+')
    if ch:
        url = 'https://www.youtube.com/results?sp=EgIQAg%253D%253D&search_query=' + quote(search_token)
    else:
        url = 'https://www.youtube.com/results?search_query=' + quote(search_token)
    it = 1
    #print(url)
    res = set()
    titles = set()
    while it <= end:
        url1 = url + '&page=' + str(it)
        #print("     ", url1)
        try:
            html = urlopen(url1)
            html = str(html.read().decode('utf-8'))
            html=html.replace('\r','')
            soup = BeautifulSoup(html, 'lxml')
        except KeyError:
            it += 1
            continue
        it += 1
        #sys.stderr.write('\n')
        a = soup.findAll('a')
        prev = None
        for _link in a:
            link = _link.get('href')
            f = False
            g = True
            if 'watch?v=' in link:
                f = True
                g = False
            if('/channel/' in link and link != prev):
                # print("       ", link)                
                if('https://www.youtube.com' in link):
                    pass
                elif('http://www.youtube.com' in link):
                    link = link.replace('http://', 'https://')                    
                else:
                    link = 'https://www.youtube.com' + link
                prev = link
                if link in IGNORE_CHANNELS:
                    continue
                if link.replace('http://', 'https://') in IGNORE_CHANNELS:
                    continue
                f = True
                res.add(link)    
            elif('/user/' in link and link != prev):
                # print("       ", link)                            
                if('https://www.youtube.com' in link):
                    pass
                elif('http://www.youtube.com' in link):
                    link = link.replace('http://', 'https://')
                else:
                    link = 'https://www.youtube.com' + link
                    link = clean_unwanted(link)
                    try:                
                        channel_link = get_channel_id(link, thread_id = -1)
                    except Exception:
                        continue
                    link = 'https://www.youtube.com/channel/%s' % channel_link
                if link in IGNORE_CHANNELS:
                    continue
                if link.replace('http://', 'https://') in IGNORE_CHANNELS:
                    continue
                f = True
                res.add(link)
            if f:
                title = _link.get('title')
                if(title == None or title == 'None' or 'Search for' in title):
                    pass
                else:
                    # print("      ", title)
                    if g:
                        f = open('/tmp/titles.txt', 'a+')
                        f.write(title + '\n')
                        f.close()
                    title = title.lower().replace('(', '').replace(')', '').replace('|', '').replace(';', '')
                    title = title.lower().replace(',', '').replace('?', '').replace('@', '').replace('!', '')
                    title = title.lower().replace('#', '').replace('$', '').replace('%', '').replace('&', '')
                    title = title.lower().replace('*', '').replace('-', '').replace('_', '').replace('=', '')
                    title = title.lower().replace('+', '').replace('{', '').replace('}', '').replace('[', '')
                    title = title.lower().replace(']', '').replace('"', '').replace(':', '').replace('.', '')
                    title = title.lstrip().rstrip().split()
                    for _ in title:
                        __ = _.lstrip().rstrip()
                        if(__ == ' ' or __ == ''):
                            pass
                        else:
                            # print(_link.get('title'))
                            # print(__)
                            titles.add(__)
    if ch:
        res1, titles1 = _add_channels_from_search_non_selenium(search_token, ch = False, end=end)
        for i in res1:
            res.add(i)
        for i in titles1:
            titles.add(i)
            #print(i)
    return res, titles

#############################################

YOUTUBE_COMMENTS_URL = 'https://www.youtube.com/all_comments?v={youtube_id}'
YOUTUBE_COMMENTS_AJAX_URL = 'https://www.youtube.com/comment_ajax'

USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'


def find_value(html, key, num_chars=2):
    pos_begin = html.find(key) + len(key) + num_chars
    pos_end = html.find('"', pos_begin)
    return html[pos_begin: pos_end]


def extract_comments(html):
    tree = lxml.html.fromstring(html)
    #print(html)
    item_sel = CSSSelector('.comment-item')
    text_sel = CSSSelector('.comment-text-content')
    time_sel = CSSSelector('.time')
    author_sel = CSSSelector('.user-name')
    for item in item_sel(tree):
        #print(author_sel(item)[0].get('href'))
        #print(text_sel(item)[0].text_content())
        #exit(0)
        s = text_sel(item)[0].text_content()
        s = s.encode('utf-8')
        #print(s)
        yield {'cid': item.get('data-cid'),
               'text': s,
               'time': time_sel(item)[0].text_content().strip(),
               'author': author_sel(item)[0].text_content(),
               'channel_id' : author_sel(item)[0].get('href')}


def extract_reply_cids(html):
    tree = lxml.html.fromstring(html)
    sel = CSSSelector('.comment-replies-header > .load-comments')
    return [i.get('data-cid') for i in sel(tree)]


def ajax_request(session, url, params, data, retries=10, sleep=20):
    for _ in range(retries):
        response = session.post(url, params=params, data=data)
        if response.status_code == 200:
            response_dict = json.loads(response.text)
            return response_dict.get('page_token', None), response_dict['html_content']
        else:
            time.sleep(sleep)


def download_comments(youtube_id, sleep=1):
    session = requests.Session()
    session.headers['User-Agent'] = USER_AGENT

    # Get Youtube page with initial comments
    response = session.get(YOUTUBE_COMMENTS_URL.format(youtube_id=youtube_id))
    html = response.text
    reply_cids = extract_reply_cids(html)

    ret_cids = []
    for comment in extract_comments(html):
        ret_cids.append(comment['cid'])
        yield comment

    page_token = find_value(html, 'data-token')
    session_token = find_value(html, 'XSRF_TOKEN', 4)

    first_iteration = True

    # Get remaining comments (the same as pressing the 'Show more' button)
    while page_token:
        data = {'video_id': youtube_id,
                'session_token': session_token}

        params = {'action_load_comments': 1,
                  'order_by_time': True,
                  'filter': youtube_id}

        if first_iteration:
            params['order_menu'] = True
        else:
            data['page_token'] = page_token

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        page_token, html = response

        reply_cids += extract_reply_cids(html)
        for comment in extract_comments(html):
            if comment['cid'] not in ret_cids:
                ret_cids.append(comment['cid'])
                yield comment

        first_iteration = False
        time.sleep(sleep)

    # Get replies (the same as pressing the 'View all X replies' link)
    for cid in reply_cids:
        data = {'comment_id': cid,
                'video_id': youtube_id,
                'can_reply': 1,
                'session_token': session_token}

        params = {'action_load_replies': 1,
                  'order_by_time': True,
                  'filter': youtube_id,
                  'tab': 'inbox'}

        response = ajax_request(session, YOUTUBE_COMMENTS_AJAX_URL, params, data)
        if not response:
            break

        _, html = response

        for comment in extract_comments(html):
            if comment['cid'] not in ret_cids:
                ret_cids.append(comment['cid'])
                yield comment
        time.sleep(sleep)

def _get_videos_by_channel_id_youtube_dl(channel_url, thread_id, channel_name, depth = 0):
    ends_at = PLAY_LIST_END
    for i in ['tv9', 'ap24x7', 'etv', 'mahaa', 'telugu', 'tv5', 'andhra', 'telangana', '10tv', 'ntvtelugu']:
        if(i in channel_name.lower().strip().replace(' ', '')):
            ends_at = 10000
    ydl_opts = {
        'format': 'bestaudio/best',
        'logger': ytdlLogger(thread_id, logwriter, channel_name),
        'playlistend' : ends_at,
        'ignoreerrors' : True
    }
    #logwriter.write('Fetching videos from: %s' % (channel_name), thread_id)
    ydl = youtube_dl.YoutubeDL(ydl_opts)
    result = ydl.extract_info(channel_url, download=False)
    videos = []
    try:
        for i in result['entries']:
            try:
                x = str(i['webpage_url']).replace('https://youtube.com/watch?v=', '')
                x = x.replace('https://www.youtube.com/watch?v=', '')
                videos.append(x)
            except Exception:
                continue
    except KeyError:
        return []
    except TypeError:
        if depth == 5:
            return []
        return _get_videos_by_channel_id_youtube_dl(channel_url, thread_id, channel_name, depth + 1)
    return videos

def _get_comments_by_video_id_non_api(youtube_id, thread_id):
    try:
        limit = 123456789
        count = 0
        comments = []
        for comment in download_comments(youtube_id):
            comment_id = comment['cid']
            try:
                author_name = comment['author'].encode('utf-8')
            except UnicodeEncodeError as e:
                pass
            author_channel_id = 'https://www.youtube.com' + comment['channel_id']
            author_channel_id = get_channel_id(author_channel_id, thread_id)
            author_channel_url = 'https://youtube.com/channel/' + author_channel_id
            author_channel_profile_image = ''
            comment_text = comment['text']#str(comment['text']).decode('utf-8')
            like_count = -1
            published_at = -1
            cur = YouTubeComment(youtube_id, comment['cid'], author_name, author_channel_id, 
                            author_channel_url, author_channel_profile_image, 
                            comment_text, like_count, published_at)
            comments.append(cur)
            count += 1
            logwriter.write('Video id: %s | Processed %d comments' % (youtube_id, count), thread_id)
        return comments
    except KeyError as e:
        print('Error:', str(e))
        sys.exit(1)


def _scrape_telugu_words():
    words = set()
    url = 'http://dsalsrv02.uchicago.edu/cgi-bin/app/brown_query.py?page='
    for i in range(1, 1410):
        try:
            html = urlopen(url + str(i))
            html = str(html.read().decode('utf-8'))
            html=html.replace('\r','')
            soup = BeautifulSoup(html, 'lxml')
        except Exception:
            continue
        HW = soup.findAll('hw')
        print(str(i) + "/1410")
        for j in HW:
            p = str(j.text)
            words.add(p)
    
    url = 'http://telugudictionary.org/telugu_english.php?id='
    for i in range(18224):
        try:
            html = urlopen(url + str(i))
            html = str(html.read().decode('utf-8'))
            html=html.replace('\r','')
            soup = BeautifulSoup(html, 'lxml')
        except Exception:
            continue
        SP = soup.findAll('span', {'class':'label-primary'})
        s = SP[1].text
        s = s.replace('Meaning of', '')
        s = s.lstrip()
        s = s.rstrip()
        if s != '' and s != ' ':
            words.add(s)
        print(str(i) + "/18224")
    url = 'http://www.indiachildnames.com/regional/telugunames.aspx?type=alpha&char='
    for char in 'ABCDEFGHIJKLMNOPQRSTUVWXYZ':
        url1 = url + char + '&pageno='
        for i in range(1, 100):
            url2 = url1 + str(i)
            try:
                #print(url2)
                html = urlopen(url2)
                html = str(html.read().decode('utf-8'))
                html=html.replace('\r','')
                soup = BeautifulSoup(html, 'lxml')
            except Exception:
                continue
            TABLE = soup.findAll('table', {'id' : 'contentstable'})
            try:
                TR = TABLE[0].findAll('tr')
                for j in TR[1:]:
                    TD = j.findAll('td')
                    s = TD[1].text
                    s = s.lstrip()
                    s = s.rstrip()
                    if s != '' and s != ' ':
                        #print(s)
                        words.add(s)
                # if 'Next Page' in html:
                #     #print('     ' + char + " " + str(i))
                #     pass
                # else:
                #     break
            except Exception:
                continue
        print(char + '/' + 'Z')
    return list(words)

# body = {
#     'id': self.total_channels_done,
#     'channel_id': channel_id
# }
# utils.es.index(index = 'idx2channels', doc_type = 'idx2channels', id = self.total_channels_done, body = body)
