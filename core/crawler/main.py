import datetime
import json
import os
import random
import sys
import threading
import time
from urllib.request import urlopen  # python3

import MySQLdb
from bs4 import BeautifulSoup
from indictrans import Transliterator
from selenium import webdriver
from selenium.common.exceptions import TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.firefox.options import Options
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

import utils
from config import *

random.seed(8595)

conn = None
cursor = []
for i in range(0): #Kept for future use.
    cursor.append(conn.cursor())

random.shuffle(CHANNELS)
random.shuffle(CHANNELS)

class ChannelUpdater(object):
    def __init__(self):
        self.total_channels_done = utils.getIndexCountES('channels')
        self.total_videos_done = utils.getIndexCountES('videos')
        self.total_comments_done = utils.getIndexCountES('comments')
        self.L = len(CHANNELS)
        self.lock = threading.Lock()
        self.channels = []
        self.channel_lock = threading.Lock()
        self.channels_done = 0
        self.cur_channels_done = set()
        self.threads_done = set()
        # self.all_videos = utils.readFile(ALL_VIDEOS_FILE, split_by='\n')
        # self.all_videos = set(self.all_videos)
        self.all_videos_lock = threading.Lock()
    
    def update(self):
        '''
        Checks if a channel has new videos and update videos accordingly.
        '''
        for i in CHANNELS:
            self.channels.append(i)
        all_threads = []
        thread_id = 0
        for _ in range(N_THREADS):
            t = threading.Thread(target=self.worker, args=(thread_id,))
            t.daemon = True
            t.start()
            all_threads.append(t)
            utils.logwriter.write('Starting thread', thread_id)
            thread_id += 1
        for t in all_threads:
            t.join()
        
        # f = open(LAST_UPDATED_FILE, 'w')
        # f.write('%s' % (str(datetime.datetime.now().timestamp())))
        # f.close()
    
    def get_channel(self, thread_id):
        res = None
        utils.logwriter.write('At M74, Acquiring lock', thread_id)
        self.channel_lock.acquire()
        while len(self.channels) > 0 and len(self.cur_channels_done) < 50:
            res = self.channels.pop()
            if not self.is_channel_done(res, thread_id):
                #self.channels_done += 1
                self.cur_channels_done.add(res)
                break
            else:
                self.total_channels_done += 1
        if res == None:
            self.threads_done.add(thread_id)
            if len(self.cur_channels_done) >= 50 and len(self.threads_done) < N_THREADS:
                if len(self.channels) > 0:
                    res = self.channels.pop()
                    self.cur_channels_done.add(res)
                    #self.channels_done += 1
        utils.logwriter.write('At M80, Releasing lock', thread_id)
        self.channel_lock.release()
        return res
    
    def present_in_processed_videos(self, v, thread_id):
        try:
            response = utils.es.get(
                index = 'videos',
                doc_type = 'videos',
                id = v
            )
            return response['found']
        except Exception as e:
            pass
            #sys.stderr.write('Exception occurred at M94: %s\n' % (str(e)))
            #sys.stderr.flush()
        return False
    
    def is_channel_done(self, channel, thread_id):
        try:
            channel = channel[::-1]
            res = ''
            for i in channel:
                if i == '/':
                    break
                else:
                    res += i
            res = res[::-1]
            channel = res
            response = utils.es.get(
                index = 'channels',
                doc_type = 'channels',
                id = channel
            )
            if response['found'] == True:
                return True
            return False
        except Exception as e:
            pass
        return False
    
    def video_worker_multi_thread(self, new_videos, channel_title, channel, url, channel_id, thread_id):
        LL = len(new_videos)
        cc = 0
        source_channel_title = channel_title
        source_channel_id = channel
        source_channel_url = url
        for jj, video in enumerate(new_videos):
            video_title = utils.get_video_title(video, thread_id)
            curL = utils.add_new_video(video_title, video, thread_id, [channel_title, jj, LL], cursor, conn, source_channel_title, source_channel_id, source_channel_url)
            cc += curL
            self.lock.acquire()
            self.total_videos_done += 1
            self.total_comments_done += curL
            self.lock.release()
            utils.logwriter.write('%s | %d/%d videos done' % (channel_title, jj + 1, LL), thread_id)    
            utils.logwriter.write('threads done: %d, Processed %d/%d channels, %d videos, %d comments' % (len(self.threads_done), self.total_channels_done, self.L, self.total_videos_done, self.total_comments_done), -1)

    def worker(self, thread_id):
        j = 0
        utils.logwriter.write('At M135, Starting', thread_id)
        while True:
            channel = self.get_channel(thread_id)
            if channel == None: #All channels were done.
                break
            channel = channel.replace('https://www.youtube.comhttps://', 'https://')
            channel = channel.replace('https://www.youtube.comhttp://', 'https://')
            channel = channel.replace('https://m.youtube.com', 'https://www.youtube.com')
            try:
                channel_id = utils.get_channel_id(channel, thread_id)
            except Exception:
                sys.stderr.write(channel + '\n')
                sys.stderr.flush()
                continue
            url = 'https://youtube.com/channel/%s' % channel_id
            channel_title = utils.get_channel_title(channel_id, thread_id)   
            utils.logwriter.write('At M102, Fetching videos', thread_id)         
            new_videos = utils._get_videos_by_channel_id_youtube_dl(url, thread_id, channel_title)
            #new_videos = utils._get_videos_by_channel_id(channel_id, '', all_videos, 0, depth=0, thread_id=thread_id)
            #new_videos = utils.get_all_channel_videos(url, thread_id)
            new_videos_tmp = []
            for v in new_videos:
                new_videos_tmp.append(v)
            new_videos = []
            patience = 0
            utils.logwriter.write('At M111, Validating videos', thread_id)         
            for v in new_videos_tmp:
                if utils.isRelatedVideo(v, thread_id) == False:
                    break
                if self.present_in_processed_videos(v, thread_id):
                #if v in self.all_videos:
                    patience += 1
                    if patience <= 10:
                        new_videos.append(v)
                    else:
                        break
                else:
                    patience = 0
                    utils.logwriter.write('At M121, Acquiring lock', thread_id)         
                    #self.all_videos_lock.acquire()
                    new_videos.append(v)
                    #self.all_videos.add(v)
                    #self.all_videos_lock.release()
            #True always
            if(utils.isRelatedChannel(new_videos, channel_title, thread_id)):
                new_videos = list(new_videos)
                random.shuffle(new_videos)
                random.shuffle(new_videos)
                random.shuffle(new_videos)
                LL = len(new_videos)
                try:
                    v1 = new_videos[:int(LL / 3)]
                    v2 = new_videos[int(LL / 3):int((2 * LL) / 3.0)]
                    v3 = new_videos[int((2 * LL) / 3):]

                except Exception as e:
                    sys.stderr.write('Error: %s, new_videos: %s\n, %d/%d/%d\n' % (str(e), str(new_videos), LL, LL / 2, LL / 2))
                    exit(0)
                th = []
                t = threading.Thread(target=self.video_worker_multi_thread, args=(v1, channel_title, channel, url, channel_id, thread_id,))
                t.daemon = True
                t.start()
                th.append(t)
                t = threading.Thread(target=self.video_worker_multi_thread, args=(v2, channel_title, channel, url, channel_id, thread_id,))
                t.daemon = True
                t.start()
                th.append(t)
                t = threading.Thread(target=self.video_worker_multi_thread, args=(v3, channel_title, channel, url, channel_id, thread_id,))
                t.daemon = True
                t.start()
                th.append(t)
                for t in th:
                    t.join()
                f = open('/tmp/channels_done.txt', 'a+')
                f.write('https://youtube.com/channel/%s | %s\n' % (channel_id, channel_title))
                f.close()
                body = {
                    'id': channel_id,
                    'done': 'True'
                }
                utils.es.index(index = 'channels', doc_type = 'channels', id = channel_id, body = body)
                self.lock.acquire()
                self.total_channels_done = utils.getIndexCountES('channels')
                utils.logwriter.write('threads done: %d, Processed %d/%d channels, %d videos, %d comments' % (len(self.threads_done), self.total_channels_done, self.L, self.total_videos_done, self.total_comments_done), -1)
                self.lock.release()
            else:
                self.lock.acquire()
                self.total_channels_done += 1
                utils.logwriter.write('threads done: %d, Processed %d/%d channels, %d videos, %d comments' % (len(self.threads_done), self.total_channels_done, self.L, self.total_videos_done, self.total_comments_done), -1)
                self.lock.release()
            j += 1
            time.sleep(5) #Sleeping for 5 seconds.
        utils.logwriter.write('Done', thread_id)

def add_trending_channels():
    '''
    Fetches the trending channels and adds it to the database. Takes care of duplicates.
    '''
    print('Adding trending channels')
    already_present = utils.readFile(ALL_CHANNELS_FILE, split_by='\n')
    newly_fetched = utils._fetch_trending_channels('')
    already_present = already_present + newly_fetched
    already_present = set(already_present)
    f = open(ALL_CHANNELS_FILE, 'w')
    for i in already_present:
        #f.write('https://www.youtube.com/channel/%s, %s\n' % (i, utils.get_channel_title(i)))
        f.write('https://www.youtube.com/channel/%s\n' % i)
    f.close()

def add_channels_by_search():
    already_present = utils.readFile(ALL_CHANNELS_FILE, split_by='\n')
    newly_fetched = []
    print('Adding channels by search')
    for i in SEARCH_WORDS:
        print('   Adding %s' % i)
        newly_fetched = newly_fetched + utils._add_channels_from_search(i)
        already_present = already_present + newly_fetched
        already_present = set(already_present)
        f = open(ALL_CHANNELS_FILE, 'w')
        for i in already_present:
            f.write('%s\n' % i)
            #f.write('%s, %s\n' % (i, utils.get_channel_title(i)))
        f.close()
        already_present = list(already_present)
    
def add_channels():
    '''
    Adds the channels from youtube search and trending.
    '''
    add_trending_channels()
    add_channels_by_search()

def filterchannels():
    f = open(ALL_CHANNELS_FILE)
    x = f.read().strip().split('\n')
    f.close()
    f = open('/tmp/channels_with_titles.txt', 'w')
    for j, i in enumerate(x):
        channel_id = i.replace('https://www.youtube.com/channel/', '')
        print(j, i)
        channel_title = utils.get_channel_title(channel_id)
        print(channel_title)
        f.write('%s,%s\n' % (i, channel_title))
        f.flush()
    f.close()

def dump():
    f = open('/tmp/channels_with_titles.txt', 'r')
    x = f.read().strip().split('\n')
    f.close()
    f = open(ALL_CHANNELS_FILE, 'w')
    for i in x:
        f.write('%s\n' % (i.split(',')[0].strip()))
    f.close()

# FIXME: BECAREFUL WITH INDICES AFTER EACH RUN IN write_to_elastic_search().
# Don't worry. They're handled.

uc = ChannelUpdater()
uc.update()
success, _ = utils.bulk(utils.es, utils.ACTIONS, index = 'comments', raise_on_error = True)
success, _ = utils.bulk(utils.es, utils.ACTIONS1, index = 'idx2comment', raise_on_error = True)
exit(0)
########## DONE ###############
########## Below methods are for fetching channels ###############

channels_collected = set()
collected_count = 0
cache = {}

class FetchChannelRecursive(object):
    def __init__(self):
        self.lock = threading.Lock()
        self.channel_lock = threading.Lock()
        self.channels = []
        self.done = 0
    
    def update(self):
        '''
        Checks if a channel has new videos and update videos accordingly.
        '''
        #TODO: Parallel processing
        for i in CHANNELS:
            self.channels.append(i)
        all_threads = []
        thread_id = 0
        for _ in range(N_THREADS):
            t = threading.Thread(target=self.worker, args=(thread_id,))
            t.daemon = True
            t.start()
            all_threads.append(t)
            thread_id += 1
        for j, t in enumerate(all_threads):
            t.join()
    
    def get_channel(self, thread_id):
        res = None
        self.lock.acquire()
        if len(self.channels) == 0:
            res = None
        else:
            res = self.channels.pop()
        self.lock.release()
        return res
    
    def worker(self, thread_id = -1):
        i = 0
        while i < 10:
            i += 1
            channel_id = self.get_channel(thread_id)
            if channel_id == None: #All channels were done.
                return
            self.lock.acquire()
            channels_collected.add(channel_id)
            self.done += 1
            self.lock.release()
            print(channel_id, " ", collected_count, " ", self.done)
            self.channel_traversal(channel_id)
        return

    def channel_traversal(self, channel_id, lvl = 0):
        global collected_count
        if(lvl == 3):
            return
        try:
            tmp = cache[channel_id]
            return
        except KeyError:
            cache[channel_id] = True
            pass
        self.lock.acquire()
        collected_count += 1
        #print("     ", channel_id, " ", collected_count)        
        sys.stdout.flush()
        self.lock.release()
        try:
            html = urlopen(channel_id)
            html = str(html.read())
            html=html.replace('\r','')
            soup = BeautifulSoup(html, 'lxml')
        except Exception:
            return

        DIV = soup.findAll('div', {'class':'yt-lockup-content'})
        res = []
        prev = None
        for div in DIV:
            a = div.findAll('a')
            for _link in a:
                link = _link.get('href')
                if(channel_id in link or link in channel_id or '/channel/null' in link):
                    continue
                if('/channel/' in link and link != prev):
                    if('https://www.youtube.com' in link):
                        pass
                    elif('http://www.youtube.com' in link):
                        link = link.replace('http://', 'https://')                    
                    else:
                        link = 'https://www.youtube.com' + link
                    prev = link
                    res.append(link)
                    self.lock.acquire()
                    channels_collected.add(link)
                    self.lock.release()
                    
                elif('/user/' in link and link != prev):
                    if('https://www.youtube.com' in link):
                        pass
                    elif('http://www.youtube.com' in link):
                        link = link.replace('http://', 'https://')
                    else:
                        link = 'https://www.youtube.com' + link
                    link = utils.clean_unwanted(link)
                    try:                
                        channel_link = utils.get_channel_id(link)
                    except Exception:
                        continue
                    res.append('https://www.youtube.com/channel/%s' % channel_link)
                    self.lock.acquire()
                    channels_collected.add('https://www.youtube.com/channel/%s' % channel_link)
                    self.lock.release()
        for i in res:
            self.channel_traversal(i, lvl=lvl + 1)


class FetchChannelsBasedOnWords(object):
    def __init__(self, words):
        self.lock = threading.Lock()
        self.channel_lock = threading.Lock()
        self.channels = set()
        self.words = words
        self.done = 0
        self.total = len(words)
        self.trn = Transliterator(source='tel', target='eng', build_lookup = True)
    
    def update(self):
        all_threads = []
        thread_id = 0
        for _ in range(N_THREADS):
            t = threading.Thread(target=self.worker, args=(thread_id,))
            t.daemon = True
            t.start()
            all_threads.append(t)
            thread_id += 1
        for j, t in enumerate(all_threads):
            t.join()
    
    def get_words(self, thread_id):
        res = None
        self.channel_lock.acquire()
        if len(self.words) == 0:
            res = None
        else:
            res = self.words.pop()
        self.channel_lock.release()
        return res
    
    def worker(self, thread_id = -1):
        while True:
            _word = self.get_words(thread_id)
            if _word == None:
                return
            word = 'తెలుగు ' + _word
            channels = utils._add_channels_from_search_non_selenium(word)
            for i in channels:
                self.lock.acquire()
                self.channels.add(i)
                self.lock.release()
            self.lock.acquire()
            self.done += 1
            self.lock.release()
            word = 'తెలుగు ' + self.trn.transform(_word)
            channels = utils._add_channels_from_search_non_selenium(word)
            for i in channels:
                self.lock.acquire()
                self.channels.add(i)
                self.lock.release()
            self.lock.acquire()
            self.done += 1
            self.lock.release()
            if(self.done % 1 == 0):
                print(str(self.done) + "/" + str(self.total) + " (%.6f%%)" % (float(self.done) / self.total))

class FetchChannelsBasedOnTitles(object):
    def __init__(self, words):
        self.lock = threading.Lock()
        self.channel_lock = threading.Lock()
        self.channels = set()
        self.words = words
        self.done = 0
        self.pres = {}
        self.already = {}
        for i in self.words:
            self.pres[i] = True
        self.trn = Transliterator(source='tel', target='eng', build_lookup = True)
        self.trn1 = Transliterator(source='eng', target='tel', build_lookup = True)
    
    def update(self):
        all_threads = []
        thread_id = 0
        for _ in range(N_THREADS):
            t = threading.Thread(target=self.worker, args=(thread_id,))
            t.daemon = True
            t.start()
            all_threads.append(t)
            thread_id += 1
        for j, t in enumerate(all_threads):
            t.join()
    
    def get_words(self, thread_id):
        res = None
        self.channel_lock.acquire()
        if len(self.words) == 0:
            res = None
        else:
            res = self.words.pop()
        self.channel_lock.release()
        return res
    
    def put_words(self, word, thread_id):
        self.channel_lock.acquire()
        try:
            tmp = self.pres[word]
        except KeyError:
            self.pres[word] = True
            self.words.append(word)
        self.channel_lock.release()
    
    def worker(self, thread_id = -1):
        ___ = 0
        while (___ <= 1000) and (self.done <= CHANNELS_LIMIT):
            if self.done >= 50000:
                end = 5
                if self.done >= CHANNELS_LIMIT:
                    break
            else:
                end = 10
            _word = self.get_words(thread_id)
            if _word == None:
                ___ += 1
                time.sleep(2)
                continue
            else:
                ___ = 0
            word = 'తెలుగు ' + _word
            #print(word)
            self.lock.acquire()
            f = open('/home/chaitanya/.youtube/words_log.txt', 'a+')
            f.write("%s\n" % (word))
            f.close()            
            self.lock.release()
            #print(word)
            channels, titles = utils._add_channels_from_search_non_selenium(word, ch = True, end=end)
            #print(channels)
            for i in channels:
                self.lock.acquire()
                if self.done < CHANNELS_LIMIT:
                    self.channels.add(i)
                    try:
                        tmp = self.already[i]
                    except KeyError:
                        self.already[i] = True
                        self.done += 1                
                    
                self.lock.release()
            word = 'telugu ' + self.trn.transform(_word)
            # self.lock.acquire()
            # f.write("%s\n" % (word))
            # self.lock.release()
            #channels = utils._add_channels_from_search_non_selenium(word)
            
            channels, titles = utils._add_channels_from_search_non_selenium(word, ch = True, end=end)
            for i in channels:
                self.lock.acquire()
                if self.done < CHANNELS_LIMIT:
                    self.channels.add(i)
                    try:
                        tmp = self.already[i]
                    except KeyError:
                        self.already[i] = True
                        self.done += 1   
                self.lock.release()
            word = 'తెలుగు ' + self.trn1.transform(_word)
            # f.write("%s\n" % (word))
            channels, titles = utils._add_channels_from_search_non_selenium(word, ch = True, end=end)
            for i in channels:
                self.lock.acquire()
                if self.done < CHANNELS_LIMIT:
                    self.channels.add(i)
                    try:
                        tmp = self.already[i]
                    except KeyError:
                        self.already[i] = True
                        self.done += 1   
                self.lock.release()
            self.lock.acquire()
            for i in titles:
                self.put_words(i, thread_id)
            self.lock.release()
            print(str(self.done) + "/%d (%.6f%%)" % (CHANNELS_LIMIT, ((self.done * 100.0) / CHANNELS_LIMIT)))
    
base_words = ['news', 'channels', 'chandra babu naidu', 'phalalu', 'andhra pradesh', 'thank you cm sir', 'pawan kalyan', 'tech', 'movies', 'songs', 'telangana']

fetcher = FetchChannelsBasedOnTitles(base_words)
fetcher.update()
f = open(ALL_CHANNELS_FILE, 'w')
for i in fetcher.channels:
   f.write('%s\n' % (i))
f.close()
