
#Youtube API Key path
API_KEY_FILE = '/home/chaitanya/.youtube/API_KEYS.txt'
ALL_VIDEOS_FILE = '/home/chaitanya/.youtube/allvideos.txt'
ALL_COMMENTS_IDS_FILE = '/home/chaitanya/.youtube/allcomments_ids.txt'
ALL_COMMENTS_FILE = '/home/chaitanya/.youtube/allcomments.txt'
ALL_CHANNELS_FILE = '/home/chaitanya/.youtube/allchannels.txt'
PATIENCE = 10 #maximum number of videos/comments to consider that are already parsed before.
USER_AGENT = 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/48.0.2564.116 Safari/537.36'
N_THREADS = 64
PLAY_LIST_END = 1000
COMMENTS_LIMIT = 10**6
REPLIES_COUNT_THRESHOLD = 20 #If the replies are >= this value, then fetch them. Else ignore.
LAST_UPDATED_DIR = '/home/chaitanya/.youtube/lastupdated/'
LAST_UPDATED_FILE = '/home/chaitanya/.youtube/lastupdated.txt'
WORDS_VOCAB_FILE = '/home/chaitanya/.youtube/words.txt'
CHANNELS_LIMIT = 100000

# Handles list of channels to parse.
# Hard coded must include channels
CHANNELS = [
    'https://www.youtube.com/channel/UCJi8M0hRKjz8SLPvJKEVTOg',
    'https://www.youtube.com/channel/UCPXTXMecYqnRKNdqdVOGSFg',
    'https://www.youtube.com/channel/UCNZOrs1QBt8cJnv9ud96qRA',
    'https://www.youtube.com/channel/UCpbW96xFi7vbTM98vGMQh-g',
    'https://www.youtube.com/channel/UC_2irx_BQR7RsBKmUV9fePQ',
    'https://www.youtube.com/channel/UCDKjhgRoPF1CQk7HluMz23A',
    'https://www.youtube.com/channel/UCNApqoVYJbYSrni4YsbXzyQ',
    'https://www.youtube.com/channel/UCnSqxrSfo1sK4WZ7nBpYW1Q',
    'https://www.youtube.com/channel/UCLiwEQsSEmXe4ShFVJk_hAQ',
    'https://www.youtube.com/channel/UCfrNV2iJ9LnHcOsdyhYyYew',
]

IGNORE_CHANNELS = [
    'https://www.youtube.com/channel/UC-9-kyTW8ZkZNDHQJ6FgpwQ',
    'https://www.youtube.com/channel/UCEgdi0XIXXZ-qJOFPf4JSKw',
    'https://www.youtube.com/channel/UCOpNcN46UbXVtpKMrmU4Abg',
    'https://www.youtube.com/channel/UClgRkhTL3_hImCAmdLfDE4g',
    'https://www.youtube.com/channel/UCY9Jh3SK0N1SAVJi-U--Rwg',
    'https://www.youtube.com/channel/UCYfdidRxbB8Qhf0Nx7ioOYw',
    'https://www.youtube.com/channel/UC4R8DWoMoI7CAwX8_LjQHig',
    'https://www.youtube.com/channel/UCvScgo6mAvbMEjszK4sSj6g',
    'https://www.youtube.com/channel/UCzuqhhs6NWbgTzMuM09WKDQ'
]

SEARCH_WORDS = [
    'telugu tech channels',
    'telugu news channels',
]

REQUIRED_KEY_WORDS = [
    'telugu',
    'news',
    'ap24x7',
    'tv9',
    'abn',
    'mahaa',
    'etv',
    'tech',
    'andhra',
    'telangana',
    'movie',
    'movies',
]

try:
    f = open(ALL_CHANNELS_FILE)
    CHANNELS1 = f.read().strip().split('\n')
    f.close()
except IOError:
    CHANNELS1 = []

CHANNELS = CHANNELS + CHANNELS1
CHANNELS = sorted(list(set(CHANNELS)))
